
#include<stdio.h> 
int input_size()
{
    int x;
    printf("\n Enter the size of the array: ");
    scanf("%d",&x);
    return x;
}

int input_tobesearched()
{
    int y;
    printf("\n Enter the element whose position is to be found : ");
    scanf("%d",&y);
    return y;
}
    

int search(int a[20],int x, int n)
{
    int temp=-1;
    for (int i=0; i<n; i++)
    {
        if (a[i]==x)
        { 
            temp=i;
        }
    }
    return temp;
}

void display(int pos)
{
    if (pos==-1)
    {
        printf("\n ERROR ... THE VALUE YOU HAVE ENTERED IS NOT PRESENT IN THE ARRAY.");
    }
    else
    {
        printf("\n Position of element is %d", pos);
    }
}

int main()
{
    int a[20], n, x, pos, i;
    n=input_size();
    printf("\n Enter the elements of the array: ");
    for(i=0; i<n; i++)
    {
        printf("\n Enter a[%d] : ", i);
        scanf("%d",&a[i]);
    }
    x=input_tobesearched();
    pos=search(a,x,n);
    display(pos);
    return 0;
}

