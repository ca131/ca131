#include<stdio.h> 
void input(int *n, int A[30])
{
	printf("\n Enter the size of the array: ");
	scanf("%d", n);
	printf("\n Enter the elements of the array: ");
	for (int i=0; i< *n; i++)
	{
		printf("\n Enter A[%d]  ", i);
		scanf("%d",&A[i]);
	}
}

int sumof(int n, int A[30])
{
	int sum=0;
	for (int i=0; i<n; i++)
	{
		sum=sum+A[i];
	}
	return sum;
}
float average(int n, int sum)
{
	float avg=sum/n;
	return avg;
}
void display(int n, int A[30], float avg)
{
	for (int i=0; i<n; i++)
	{
		printf("\n A[%d] = %d", i, A[i]);
	}
	printf("\n The average of all elements of the array = %.2f", avg);
}
int main()
{
	int n, A[30], sum=0;
	float avg=0;
	input(&n,A);
	sum = sumof(n,A);
	avg = average(n,sum);
	display(n,A,avg);
	return 0;
}
