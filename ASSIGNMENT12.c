#include<stdio.h> 
struct complex
{
    float re;
    float im;
};

void input(struct complex *c1, struct complex *c2)
{
    printf("\n Enter real part of number 1 : ");
    scanf("%f", &c1->re);
    printf("\n Enter imaginary part of number 1 : ");
    scanf("%f", &c1->im);
    printf("\n Enter real part of number 2 : ");
    scanf("%f", &c2->re);
    printf("\n Enter imaginary part of number 2 : ");
    scanf("%f", &c2->im);
}

void compute(struct complex c1, struct complex c2, struct complex *c3)
{
    (*c3).re = c1.re + c2.re;
    (*c3).im = c1.im + c2.im;
}

void display(struct complex c1, struct complex c2, struct complex c3)
{
    printf("\n C1 = %.2f + %.2f i", c1.re, c1.im);
    printf("\n C2 = %.2f + %.2f i", c2.re, c2.im);
    printf("\n C1 + C2 = C3 = %.2f + i%.2f ", c3.re, c3.im);
}

int main()
{
    struct complex c1, c2, c3;
    input(&c1, &c2);
    compute(c1, c2, &c3);
    display(c1, c2, c3);
    return 0;
}


